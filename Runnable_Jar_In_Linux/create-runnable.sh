#!/bin/sh
#Declaration
MAIN_CLASS=Server
MAIN_PACKAGE=dharam.util.share.
FILE_NALE=share

JAR_FILE=$FILE_NALE.jar
RUN_FILE=$FILE_NALE.run

#Execution
if test ! -f "stub.sh"; then
	echo "Creating stub.sh file";
	echo "#!/bin/sh
ME=\`which \"\$0\" 2>/dev/null\`
[ \$? -gt 0 -a -f \"\$0\" ] && ME=\"./\$0\"
java=java
if test -n \"\$JAVA_HOME\"; then
	java=\"\$JAVA_HOME/bin/java\"
fi
exec \"\$java\" \$java_args -jar \$ME \"\$@\"
exit \$0" >> stub.sh;
fi

echo "creating target dir..."
mkdir target

echo "compiling..."
javac -d ./target src/**.java

cd target

echo "Creating manifest..."
echo Main-Class: $MAIN_PACKAGE$MAIN_CLASS > MANIFEST.MF

echo "Creating jar..."
jar -cvmf MANIFEST.MF $JAR_FILE ./**

cd ..

echo "Creating run..."
cat stub.sh target/$JAR_FILE > $RUN_FILE

# echo "Removing stub file"
# rm stub.sh

echo "Removing target dir"
rm -r target

echo "Giving executable permission..."
chmod +x $RUN_FILE

# echo "Done, Running file for test..."
# ./$RUN_FILE

echo "Done."
exit $?
