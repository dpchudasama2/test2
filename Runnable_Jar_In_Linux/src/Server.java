package dharam.util.share;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

import java.time.LocalDate;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

/** Util class which provide server type services
 * @author Dharmendrasinh Chudasama

 * @implSpec <code><pre>
 * 	new Server()
 * 	.sharePath("/repository", "/home/dchudasama/.m2/custom-remote-repository")
 * 	.addFixPath("/owner", w->Response.generate("Dharmendrasinh chudasama"))
 * 	.start();
 * </pre></code>
 */
public class Server {

	private HttpServer server;
	private PrintStream out = System.out;

	/** Create instance with default port 8000
	 * @throws IOException
	 */
	public Server() throws IOException {
		this(8000);
	}

	public Server(int port) throws IOException {
        this.server = HttpServer.create(new InetSocketAddress(port), 0);
	}
	
	public void start(){
		this.addFixPath("/stop", wrapper->{
			wrapper.setOnDone(()->server.stop(0));
			out.println("Closing server, bye...");
			return Response.generate("Closing server, bye...");
		});

		server.start();
	}

	//==============================================================================================
	/** Create contextPath
	 * @param contextPath will cover up urls "&lt;protocol&gt;://&lt;host&gt;:&lt;port&gt;/&lt;contextPath&gt;/**"
	 * @param function
	 * @return current instance for chaining
	 */
	public Server createContext(String contextPath, Function<HandlerWrapper, Response> function){
        server.createContext(contextPath, httpExchange->{
        	HandlerWrapper wrapper = new HandlerWrapper(httpExchange);
        	
			try(OutputStream respOutStream = httpExchange.getResponseBody()) {
				try {
					function.accept(wrapper).write(wrapper, respOutStream);
				} catch (Exception e) {
					Response.error(500, "Server error !", e).write(wrapper, respOutStream);
					e.printStackTrace(out);
				/*	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(800);
        			PrintStream printStream = new PrintStream(byteArrayOutputStream);
        			printStream.println("Server error !");
					e.printStackTrace(printStream);
        			
					String content = byteArrayOutputStream.toString();
					Response.error(500, content).write(wrapper, respOutStream);
					out.print(content);*/
/*	        		httpExchange.sendResponseHeaders(500, 0);
	        		Stream.of(out, new PrintStream(respOutStream)).forEach(printStream->{
	        			printStream.println("500: Server error !");
	        			e.printStackTrace(printStream);
	        		});*/
				}
        	}
        	
        	wrapper.done();
        });
        out.println("Created context: \"http://localhost:"+server.getAddress().getPort()+contextPath+"\"");
        return this;
	}

	/** Bind path with local file system
	 * @param contextPath
	 * @param localFilePath
	 * @return current instance for chaining
	 * @throws FileNotFoundException - in case of not found local file
	 */
	public Server sharePath(String contextPath, String localFilePath) throws FileNotFoundException {
		File file = new File(localFilePath);
		if(!file.exists())
			throw new FileNotFoundException("local file path not exists");
		return createContext(contextPath, wrapper->Response.generate(new File(file, wrapper.getURI())));
	}
	//==============================================================================================
	private Map<String, Function<HandlerWrapper, Response>> fixPathMap = null;
	/** Create/append fix path/, if not found then returns appropriate message to browser<br/>
	 * here, contextPath = "/fix"
	 * @param fixPath will cover up only url "&lt;protocol&gt;://&lt;host&gt;:&lt;port&gt;/&lt;contextPath&gt;/&lt;fixPath&gt;"
	 * @param function
	 * @return current instance for chaining
	 */
	public Server addFixPath(String fixPath, Function<HandlerWrapper, Response> function){
		final String contextPath = "/fix";
		if(fixPathMap == null){
			fixPathMap = new HashMap<>();
			createContext(contextPath, wrapper->{
				Function<HandlerWrapper, Response> handler = fixPathMap.get(wrapper.getURI());
				if(handler==null)
					handler = w->Response.error(404, "Response not found");
				return handler.accept(wrapper);
			});
		}
		fixPathMap.put(fixPath, function);
        out.println("\t   Path: \"http://localhost:"+server.getAddress().getPort()+contextPath+fixPath+"\"");
		return this;
	}

	//=============================== SUPPORTERS =================================================
	/** Class which provides static methods for generate response
	 * @author Dharmendrasinh Chudasama
	 * @see #generate(String)
	 * @see #generate(File)
	 * @see #generate(int, String)
	 */
	public static abstract class Response {
		public abstract void write(HandlerWrapper wrapper, OutputStream outStrm) throws IOException;
		
		/** Generate simple text message
		 * @param content
		 * @return
		 */
		public static Response generate(String content){
			return generate(200, content);
		}

		public static Response error(int errorCode, String message){
			return error(errorCode, message, null);
		}
		public static Response error(int errorCode, String message, Exception ex){
//			return generate(errorCode, errorCode+": "+message);
			return new Response(){
				@Override
				public void write(HandlerWrapper wrapper, OutputStream outStrm) throws IOException {
					StringBuilder data = new StringBuilder();
					data.append("<html>").append("<body>");
					data.append("<p>").append("<a href='/fix/stop'>STOP</a>\n").append("</p>");
					data.append("<h1 style=\"color:red;\">");
					data.append(errorCode).append(": ").append(message);
					data.append("</h1>");
					data.append("<p>").append("Author: Dharmendrasinh P. Chudasama").append("</p>");
					if (ex != null) {
						data.append("<pre>");
						StringWriter sw = new StringWriter();
						ex.printStackTrace(new PrintWriter(sw));
						data.append(sw.toString());
						data.append("</pre>");
					}
					data.append("</body>").append("</html>");
					
					String content = data.toString();
//					String content = errorCode+": "+message;
					wrapper.getHttpExchange().sendResponseHeaders(errorCode, content.length());
	        		outStrm.write(content.getBytes());
				}
			};
		}

		private static Response generate(int statusCode, String content){
			return new Response(){
				@Override
				public void write(HandlerWrapper wrapper, OutputStream outStrm) throws IOException {
					wrapper.getHttpExchange().sendResponseHeaders(statusCode, content.length());
	        		outStrm.write(content.getBytes());
				}
			};
		}
		
		/** Fetch data from input stream and resend it in response
		 * @param inputStream
		 * @return
		 */
		public static Response generate(InputStream inputStream){
			return new Response(){
				@Override
				public void write(HandlerWrapper wrapper, OutputStream outStrm) throws IOException {
					wrapper.getHttpExchange().sendResponseHeaders(200, 0);
					byte[] buf = new byte[1024]; int i; 
					while((i=inputStream.read(buf)) > 0)
						outStrm.write(buf, 0, i);
					outStrm.flush();
				}
			};
		}

		/** send file data as response
		 * @param file
		 * @return
		 */
		public static Response generate(File file){
			return new Response(){
				@Override
				public void write(HandlerWrapper wrapper, OutputStream outStrm) throws IOException {
					if(file==null || !file.exists())
						error(404, "File not found").write(wrapper, outStrm);
					else if(!file.canRead())
						error(403, "Forbidden: Can not read file").write(wrapper, outStrm);
					else if(file.isFile()){
						wrapper.getHttpExchange().sendResponseHeaders(200, file.length());
		        		Files.copy(file.toPath(), outStrm);
					}
					else if(file.isDirectory()){
						String line = "<br/>\n";

						StringBuilder content = new StringBuilder();
						content.append("<html><body>\n");
						
						String urlPath0 = wrapper.getURLPath();
						String urlPath = urlPath0.endsWith("/") ? urlPath0.substring(0,urlPath0.length()-1) :urlPath0;
						content
							.append("<a href='"+urlPath+"/..'>UP</a>\n").append(" | ")
//							.append("<a href='/'>HOME</a>\n").append(" | ")
							.append("<a href='/fix/stop'>STOP</a>\n").append(line)
							;
						content.append("<p>").append("Author: Dharmendrasinh P. Chudasama").append("</p>");
						content.append(line);
						
						content.append(wrapper.getURI()).append(line);
						content.append(line);
						
						content.append("<ul>");
						Stream.of(file.list())
							.map(name->"<a href='"+urlPath+'/'+name+"'>"+name+"</a>"+line)
							.forEachOrdered(data->content.append("<li>"+data+"</li>\n"));
						content.append("</ul>");
						
						content.append("<body><html>");
						generate(content.toString()).write(wrapper, outStrm);
					}
					else
						error(404, "Invalid response").write(wrapper, outStrm);
				}
			};
		}
		
	}

	class HandlerWrapper {
		private HttpExchange httpExchange;
		private Runnable onDone = null;

		private HandlerWrapper(HttpExchange httpExchange){
			this.httpExchange = httpExchange;
		}
		
		public void setOnDone(Runnable onDone) {
			this.onDone = onDone;
		}

		public void done() {
			if (onDone != null)
				onDone.run();
		}

		public HttpExchange getHttpExchange() {
			return httpExchange;
		}
		
		public String getURI() throws UnsupportedEncodingException {
        	String urlPath = getURLPath();
			int contextPathLen = getContextPath().length();
			String uri = urlPath.substring(contextPathLen);
			int paramIndex = uri.indexOf('?');
			return paramIndex==-1 ? uri : uri.substring(0, paramIndex);
		}

		public String getURLPath() throws UnsupportedEncodingException {
			return httpExchange.getRequestURI().getPath();
//			return URLDecoder.decode(httpExchange.getRequestURI().toString(), "UTF-8");
		}
		
		private Map<String, String> paramMap = null;
		public Map<String, String> getParamMap() throws Exception {
			if(paramMap == null){
				String path = httpExchange.getRequestURI().toString();
				int i = path.indexOf('?');
				if(i==-1)
					paramMap = Collections.emptyMap();
				else {
					String uri = path.substring(i+1);
					
					Map<String, String> map = new LinkedHashMap<String, String>();
//					Function<String,String> decode = src->URLDecoder.decode(src, "UTF-8");
					for(String pair : uri.split("\\&")) {
						int index = pair.indexOf('=');
						if(index==-1)
							map.put(decode(pair), "");
						else {
							String key = pair.substring(0, index);
							String value = pair.substring(index+1);
							map.put(decode(key), decode(value));
						}
					};
					paramMap = map;
				}
			}

			return paramMap;
		}
		
		private String decode(String src) throws UnsupportedEncodingException {
			return URLDecoder.decode(src, "UTF-8");
		}

		public String getParam(String key) throws Exception {
			return getParamMap().get(key);
		}
		
		public String getParam(String key, String defaultValue) throws Exception {
			return getParamMap().getOrDefault(key, defaultValue);
		}
		
		public String getContextPath(){
			return getContext().getPath();
		}

		public HttpContext getContext() {
			return httpExchange.getHttpContext();
		}
		
		public OutputStream getRespBody(){
			return httpExchange.getResponseBody();
		}
	}

	@FunctionalInterface public interface Function<T,R>{ R accept(T data) throws Exception; }
	
	public static void main(String[] args) throws IOException {

		boolean isAlive = LocalDate.now().isBefore(LocalDate.of(2018, 9, 3));
		if(isAlive){
			int port = Integer.parseInt(System.getProperty("port", "8000"));
			String shareContext = System.getProperty("shareContext", "/share");
			String shareDir = System.getProperty("shareDir", "./");

			new Server(port).sharePath(shareContext, shareDir).start();

			System.out.println("Sharing path = "+shareDir);
		}
		else {
			System.out.println("Expired...");
			return;
		}
/*
		//Example 1
		new Server()
		.sharePath("/repository", "/home/dchudasama/.m2/custom-remote-repository")
		.start();

		//Example 2
		new Server()
		.createContext("/uri", w->{
			return Response.generate(w.getURI());
		})
		.createContext("/param", w->{
			String name = w.getParam("name", "Unknown");
			return Response.generate(name);
		})
		.sharePath("/repository", "/home/dchudasama/.m2/custom-remote-repository")
		.addFixPath("/ddd", w->Response.generate("Dharmendrasinh chudasama"))
		.start();
*/
	}
}
