package dharm.test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellAddress;

import com.capitaworld.sidbi.integration.domain.eligibility.EligibilityDetail;

import dharm.test.util.doc.ExcelReader;
import dharm.test.util.doc.ExcelReader.CellWrapper;
import dharm.test.util.doc.ExcelReader.RowWrapper;

public class ExcelWork {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		File file = new File("E:\\CW-Fork\\service-sidbi-integration\\Document\\Eligibility Data.xlsx");
//		File file = downloadedFile("Data Fields for API Process v1.6_BOI - Copy.xlsx");
		
		ExcelReader reader = getExcel(file, 2);
//		reader.addStreamInterceptor(filterYellowCol('C'));
//		reader.addStreamInterceptor(s->s.skip(520));
		reader.addStreamInterceptor(colValFilter('B', v->!v.isEmpty() && Pattern.matches("[A-Za-z]{1,2}", v)));
		/*
		 * reader.addStreamInterceptor(s->s.peek(rw->{
		 * Optional.of(rw.getCellWrapper('F')) .map(CellWrapper::getCell) //
		 * .ifPresent(c->System.out.println(c.getSheet().getSheetName())); ; }));
		 */
//		reader.addStreamInterceptor(s->s.ma);
//		reader.addStreamInterceptor(colValShould('G', "current_fin_arrang_det"));
//			.getAll(file, 25)
//			.getAll(10)
//			.stream().map(gstIn->qg.application(null, null, null, gstIn)).forEach(System.out::println);

		Set<String> cols = new ReflectionUtils()
			.getEntityInfo(EligibilityDetail.class)
			.getEntityFieldInfoStream()
			.map(fi->fi.getDBColName())
			.collect(Collectors.toSet());
			
		reader.addStreamInterceptor(s->s
			.peek(rw->{
				CellWrapper cw = rw.getCellWrapper('G');
				String val = cw.getValue();
//				CellAddress address = cw.getCell().get().getAddress();
				CellAddress address = cw.getCell().orElseGet(()->{
					Cell cell = rw.getRow().createCell('G'-'A');
					cell.setCellValue("---");
					System.out.println("created "+cell.getAddress());
					return cell;
				}).getAddress();
				if(cols.contains(val))
					System.out.println("Found "+address+": "+val);
				else
					throw new IllegalArgumentException("Not found "+address+": val="+val);
			})
		);
//		reader.shouldSave();

//		System.out.println(reader.getCol('H').size());
		System.out.println();
		
		reader.getCol('G');
//			.stream()
//			.map(str->createEntityCode(str))
//			.forEach(System.out::println);
		
		System.out.println("Done");
		
//		System.out.println(buf);
	}

	private static UnaryOperator<Stream<RowWrapper>> colValShould(char colChar, String valueShould) {
		return colValFilter(colChar, valueShould::equalsIgnoreCase);
	}

	private static UnaryOperator<Stream<RowWrapper>> colValFilter(char colChar, Function<String, Boolean> valFilterFun) {
		return stream->stream
				.filter(r->valFilterFun.apply(r.getCellWrapper(colChar).getValue()));
	}

	/**
	 * @param file
	 * @param sheetIndex base=0
	 * @return instance
	 */
	private static ExcelReader getExcel(File file, int sheetIndex) {
		return new ExcelReader(file).setSheetIndex(sheetIndex);
	}

	/**
	 * @param filename
	 * @return file from download directory
	 */
	private static File downloadedFile(String filename) {
		return new File("C:\\Users\\dharmendra.chudasama\\Downloads", filename);
	}

	private static final Pattern pattern = Pattern.compile("(?<word>[A-Za-z]+|\\d+)");
	private static final String empty = "\t// Empty ";
	private static String createEntityCode(String dataStr) {
		String val = dataStr;
		int i = val.indexOf('.');
		if(i != -1 && i < 5)
			val = val.substring(i+1);
		for(char ch : ".([".toCharArray()) {
			i = val.indexOf(ch);
			if(i != -1)
				val = val.substring(0, i);
		}
//			val = val.substring(val.indexOf('.')+1);
//			val = val.substring(val.indexOf('(')+1);
//			val = val.substring(val.indexOf('[')+1);
		if(val.isEmpty()) return empty+"\r\n";
		
		Matcher matcher = pattern.matcher(val);
		StringBuilder buf = new StringBuilder();
		StringJoiner joiner = new StringJoiner("_");
		try {
			while(true) {
				getGroups(matcher, (k,v)->{
					if(v.length()==0) return;
					
					joiner.add(v.toLowerCase());
					
					char f = v.charAt(0);
					f = buf.length()==0 ? Character.toLowerCase(f) : Character.toUpperCase(f);
					buf.append(f).append(v.substring(1));
				});
			}
		} catch (ReflectiveOperationException | IllegalArgumentException e) {}

		String col_name = joiner.toString();
		if(col_name.length() > 30)
			col_name += " ==len["+col_name.length()+"] > 30==";//throw new RuntimeException("Out of length(30): < ("+col_name.length()+") : "+col_name);
		return buf.length()==0 ? empty+':'+dataStr+"\r\n" : "\t@Column(name=\""+col_name+"\") //"+dataStr+"\r\n\tprivate String "+buf+";\r\n";
	//		return joiner.toString(); //db_col_name
	//	return buf.toString(); //varName
	}

	private static UnaryOperator<Stream<RowWrapper>> filterYellowCol(char cellChar) {
		return stream->stream
			.filter(rw->{
				java.awt.Color c = rw.getCellWrapper(cellChar).getBGColor(null);
				return c!=null && c.getRed()==255 && c.getGreen()==255 && c.getBlue()==0; //yellow
			})
//			.skip(2).limit(73)
			;
	}

	static void getGroups(Matcher matcher, BiConsumer<String, String> matchedConsumer) throws ReflectiveOperationException {
		if(!matcher.find())
			throw new IllegalArgumentException("Assertion failed: "+"Pattern does not matched");

		Method namedGroupsMethod = Pattern.class.getDeclaredMethod("namedGroups");
		namedGroupsMethod.setAccessible(true);

		@SuppressWarnings("unchecked")
		Map<String, Integer> namedGroups = (Map<String, Integer>) namedGroupsMethod.invoke(matcher.pattern());
		namedGroups.keySet().forEach(nm->matchedConsumer.accept(nm, matcher.group(nm)));
	}

}
