package dharm.test.util;

import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

class CryptoUtils {
	private final static String ENC_ALGO = "AES/CBC/PKCS5Padding";
	private final static String ENC_KEY_ALGO = "AES";
//	private final static byte[] keyValue = "Test1Iss@Keyy@2018seCKey".getBytes(); //
	private final static byte[] keyValue = {'T', 'e', 'S', 't', '1', 'A', 'i', 'Z', 'Z', '#', 's', 'e', 'C', 'K', 'e', 'y', '@', '2', '0', '0', '0', '$', 'b', '2', 'b', '#', 's', 'e', 'C', 'K', 'e', 'y'};

	public static String encrypt(final String plainData) throws GeneralSecurityException {
		Cipher c = getCipher(Cipher.ENCRYPT_MODE);
		byte[] encVal = c.doFinal(plainData.getBytes());
		return DatatypeConverter.printBase64Binary(encVal);
	}

	public static String decrypt(final String encryptedData) throws GeneralSecurityException {
		Cipher c = getCipher(Cipher.DECRYPT_MODE);
		byte[] decordedValue = DatatypeConverter.parseBase64Binary(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		return new String(decValue);
	}

	private static Cipher getCipher(final int opmode) throws GeneralSecurityException {
		Key key = new SecretKeySpec(keyValue, ENC_KEY_ALGO);
		IvParameterSpec iv = new IvParameterSpec("0030004008000607".getBytes());
		Cipher c = Cipher.getInstance(ENC_ALGO);
		c.init(opmode, key, iv);
		return c;
	}
/*	
	public static void main(String[] arg) throws Exception{
		System.out.println("#Database");
		System.out.println("db_driver="+"com.mysql.jdbc.Driver");
		System.out.println("db_url="+CryptoUtils.encrypt("jdbc:mysql://192.168.7.1:3306/test_db"));
		System.out.println("db_username="+CryptoUtils.encrypt("dham"));
		System.out.println("db_password="+CryptoUtils.encrypt("Dpc@passw"));
	}
*/
}
