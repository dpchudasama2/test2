package dharm.test.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class FormPanelBuilder {
	private List<String> labelList = new ArrayList<>();
	private List<JComponent[]> componentList = new ArrayList<>();
	private int maxCol;

	public void addPair(String label, JComponent... components){
		labelList.add(label + " :");
		componentList.add(components);
		maxCol = Math.max(maxCol, components.length);
	}

	public JPanel buildPanel(){
		GridBagLayout layout = new GridBagLayout();
//		layout.columnWidths = new int[]{0, 49, 64, 0};
//		layout.rowHeights = new int[]{0, 0, 0, 0, 0};
//		layout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
//		layout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};

		GridBagConstraints labelConstraints = new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 10), 0, 0);
		GridBagConstraints componentsConstraints = new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0);		

		final JPanel panel = new JPanel(layout);
		Iterator<JComponent[]> componentItr = componentList.iterator();
		for (String label : labelList) {
			//label
			labelConstraints.gridy++;
			panel.add(new JLabel(label), labelConstraints);

			//content
			JComponent[] componentArr = componentItr.next();
			componentsConstraints.gridy++;
			componentsConstraints.gridwidth = maxCol/componentArr.length;
			for (JComponent component : componentArr){
				if (component != null)
					panel.add(component, componentsConstraints);
				else
					panel.add(Box.createGlue(), componentsConstraints);
			}
		}
		
		return panel;
	}
	
	//TODO example
	public static void main(String[] args) {
		FormPanelBuilder builder = new FormPanelBuilder();
		builder.addPair("Firstname", new JTextField("Enter your first name"));
		builder.addPair("Lastname", new JTextField("Enter your last name"));
//		builder.addPair("Sex", null, new JRadioButton("Male", true), new JRadioButton("Female"));
		builder.addPair("Sex", new JRadioButton("Male", true), new JRadioButton("Female"));
		builder.addPair("Language knowledge", new JCheckBox("Gujarati"), new JCheckBox("Hindi"), new JCheckBox("English"), new JCheckBox("Sanskrit"));
		builder.addPair("Can", new JCheckBox("Read"), new JCheckBox("Write"), new JCheckBox("A"));

		JPanel panel = builder.buildPanel();
		panel.setBorder(new LineBorder(Color.RED));
		JOptionPane.showMessageDialog(null, panel);
	}
}
