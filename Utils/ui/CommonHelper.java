package dharm.mytasks.ui.helper;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

import javax.swing.SwingUtilities;

public class CommonHelper {
	
	private CommonHelper() {
		throw new UnsupportedOperationException("Have static methods only");
	}

	/**
     * @param comp component where listener should add
     * @param onRighClickConsumer
     */
    public static void onMouseRightClick(Component comp, Consumer<MouseEvent> onRighClickConsumer){
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(SwingUtilities.isRightMouseButton(e) && e.getClickCount()==1)
                    onRighClickConsumer.accept(e); //call on right click
            }
        });
    }
}
