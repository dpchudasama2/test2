package dharm.mytasks.component;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import dharm.mytasks.ui.helper.CommonHelper;

public class Tree<T extends Object> extends JTree {
    private static final long serialVersionUID = 1L;

    public Tree(T rootUserObj){
        this(new TreeNode<>(rootUserObj));
    }

    public Tree(TreeNode<T> root) {
        super(root);

        this.setPreferredSize(new Dimension(150, 0));
//      this.setRootVisible(false);
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        this.setExpandsSelectedPaths(true);
    }

    @SuppressWarnings("unchecked")
    public TreeNode<T> getRoot() {
        return (TreeNode<T>) getModel().getRoot();
    }

    public void fillChildren(Consumer<TreeNode<T>> childFiller){
        getRoot().fillChildren(childFiller);
    }

    /** Loop for all nodes (deep=true) */
    public void forEachNodes(Consumer<TreeNode<T>> childConsumer){
        TreeNode<T> root = getRoot();
        childConsumer.accept(root);
        root.forEach(childConsumer, true);
    }

    public T getRootUserObject(){
        return getRoot().getUserObject();
    }

    public TreeNode<T> getSelectedNode(){
        @SuppressWarnings("unchecked")
        TreeNode<T> node = (TreeNode<T>) getSelectionModel().getSelectionPath().getLastPathComponent();
        return node;
    }

    public T getSelectedUserObject(){
        TreeNode<T> node = getSelectedNode();
        if(node != null /*&& node.isLeaf()*/) {
            return node.getUserObject();
        }
        return null;
    }

    public void setSelectedNode(TreeNode<T> node){
        setSelectionPath(new TreePath(node.getPath()));
    }

    public void onChange(Consumer<T> consumer){
        super.addTreeSelectionListener(evt->{
            TreePath selectedPath = evt.getNewLeadSelectionPath();
            if(selectedPath==null) return;
            @SuppressWarnings("unchecked")
            TreeNode<T> node = (TreeNode<T>) selectedPath.getLastPathComponent();
            if(node != null && node.isLeaf()) {
                T userObject = node.getUserObject();
                consumer.accept(userObject);
            }
        });
    }

    public void onMouseRightClickOnNode(BiConsumer<TreeNode<T>, MouseEvent> consumer){
        Tree<T> tree = this;
        CommonHelper.onMouseRightClick(tree, evt->{
            TreePath path = getPathForLocation(evt.getX(), evt.getY());
            if(path==null) return;
            tree.setSelectionPath(path);
            TreeNode<T> node = tree.getSelectedNode();

            consumer.accept(node, evt);
        });
    }

    public void showPopupOnRightClick(BiConsumer<MyJPopupMenu, TreeNode<T>> popupMenuItemFiller){
        this.onMouseRightClickOnNode((node,evt)->{
            MyJPopupMenu popup = new MyJPopupMenu();
            popupMenuItemFiller.accept(popup,node);
            popup.show(evt.getComponent(), evt.getX(), evt.getY());
        });
    }

    /** Reload/refresh in GUI */
    public void reload(){
        ((DefaultTreeModel) getModel()).reload();
    }

    public static class TreeNode<T> extends DefaultMutableTreeNode {
        private static final long serialVersionUID = 1L;

        public TreeNode(T userObj){
            super(userObj);
        }

        /** @param userObj
         * @return child node
         */
        public TreeNode<T> addChild(T userObj) {
            TreeNode<T> child = new TreeNode<>(userObj);
            add(child);
            return child;
        }

        @Override
        public T getUserObject(){
            @SuppressWarnings("unchecked")
            T userObject = (T) super.getUserObject();
            return userObject;
        }

        @SuppressWarnings("unchecked")
        public List<TreeNode<T>> getChildren(){
            return (List<TreeNode<T>>) children;//.clone();
        }

        public void fillChildren(Consumer<TreeNode<T>> childFiller){
            if(getChildCount() != 0)
                super.removeAllChildren();
            childFiller.accept(this);
            forEach(child->child.fillChildren(childFiller), false);
        }

        /**
         * @param childConsumer
         * @param deep <code>true</code>=access all, <code>false</code>=first level children
         */
        public void forEach(Consumer<TreeNode<T>> childConsumer, boolean deep){
            getChildren().forEach(child->{
                childConsumer.accept(child);
                if(deep)
                    child.forEach(childConsumer, deep);
            });
        }

    }
}
