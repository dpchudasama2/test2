package dharm.test.util.doc;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.json.simple.JSONObject;

/**
 * Entity class which helps to create JSON string
 * 
 * @author Dharmendrasinh Chudasama
 * @since 1.0
 * @version 1.0
 */
public class JSONWriter {
//	private Map<String, Object> map = new LinkedHashMap<>(); //for maintain sequence
	private Map<String, Object> map = new HashMap<>();
	
/*	*//** All in one method for create json string
	 * @param bodyFiller
	 * @return created JSON string
	 *//*
	public static String createJSONString(Consumer<JSONWriter> bodyFiller) {
		final JSONWriter creator = new JSONWriter();
		bodyFiller.accept(creator);
		return creator.toJSONString();
	}
*/	
	/** append body portion as child using Consumer(lambda)
	 * @param key
	 * @param bodyFiller child entity instance will be passed as argument
	 */
	public void child(String key, Consumer<JSONWriter> bodyFiller) {
		final JSONWriter child = new JSONWriter();
		bodyFiller.accept(child);
		map.put(key, child.map);
	}

	/** Fill child key value pair
	 * @return current instance
	 */
	public JSONWriter child(String key, Object value) {
		map.put(key, value);
		return this;
	}

	public String toJSONString() {
		return JSONObject.toJSONString(map);
	}
	
}
