package dharm.test.util.doc;

import java.util.Map;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Entity class which helps to read JSON string
 * 
 * @author Dharmendrasinh Chudasama
 * @since 1.0
 * @version 1.0
 */
public class JSONReader {
	private Map<String, Object> map;
	
	@SuppressWarnings("unchecked")
	public JSONReader(String jsonString) throws ParseException {
		this((Map<String, Object>) new JSONParser().parse(jsonString));
	}
	
	public JSONReader(Map<String, Object> map) {
		this.map = map;
	}
	
	/**
	 * @param key
	 * @return
	 * @throws ClassCastException if not valid child map
	 */
	public JSONReader getChildren(String key) throws ClassCastException {
		@SuppressWarnings("unchecked")
		Map<String, Object> child = (Map<String, Object>) map.get(key);
		return new JSONReader(child);
	}
	
	/**
	 * @param key
	 * @return value
	 * @throws ClassCastException if not valid child String value
	 */
	public String getChildValue(String key) throws ClassCastException {
		return (String) map.get(key);
	}

}
