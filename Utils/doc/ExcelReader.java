package dharm.test.util.doc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.ExtendedColor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/** Read data from excel
 * @author dharmendra.chudasama
 */
public class ExcelReader {
	private String filePassword = null;
	private boolean readOnly = true;
	private int sheetIndex = 0;
	private String sheetName = null;
	private Function<Cell, String> cellToStrConvertor = new DataFormatter()::formatCellValue;
	private Function<Stream<RowWrapper>, Stream<RowWrapper>> streamInterceptor = UnaryOperator.identity();
	/** Workbook pre accessor */
	private Consumer<Workbook> config = wb->wb.setMissingCellPolicy(MissingCellPolicy.CREATE_NULL_AS_BLANK);
	private File file;
	
	//------------------------- Constructor ---------------------------
	public ExcelReader(String filePath) {
		this(new File(filePath));
	}
	public ExcelReader(File file) {
		this.file = file;
	}
	//------------------------- Start setter ---------------------------
	/** set password to open file
	 * @param filePassword
	 * @return current instance
	 */
	public ExcelReader setFilePassword(String filePassword) {
		this.filePassword = filePassword;
		return this;
	}

	/** set file to open as read only or not, default <code>true</code>
	 * set readonly=false to save file at the end
	 * @param readOnly
	 * @return current instance
	 */
	public ExcelReader setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		return this;
	}

	/** default 0
	 * @param sheetIndex
	 */
	public ExcelReader setSheetIndex(int sheetIndex) {
		if(this.sheetName != null)
			throw new IllegalArgumentException("SheetName should not set when want to use sheetIndex, existing sheetName: "+this.sheetName);
		this.sheetIndex = sheetIndex;
		return this;
	}
	
	/**Default: <code>null</code>, it uses sheetIndex
	 * @param sheetName
	 */
	public ExcelReader setSheetName(String sheetName) {
		if(this.sheetIndex != 0)
			throw new IllegalArgumentException("SheetIndex should not set when want to use sheetName, existing sheetIndex: "+this.sheetIndex);
		this.sheetName = sheetName;
		return this;
	}
	
	
	/** Default: new DataFormatter()::formatCellValue
	 * @param cellToStrConvertor
	 * @return current instance
	 */
	public ExcelReader setCellToStrConvertor(Function<Cell, String> cellToStrConvertor) {
		this.cellToStrConvertor = cellToStrConvertor;
		return this;
	}
	
	public ExcelReader setStreamInterceptor(UnaryOperator<Stream<RowWrapper>> filter) {
		this.streamInterceptor = filter;
		return this;
	}

	/** Example: <code><pre> reader.addStreamInterceptor(stream->stream
	 * 		.skip(1)
	 * 		.limit(10)
	 * 		.filter(rw->!rw.getCellVal(0).isEmpty())
	 * )</pre></code>
	 * @param filter
	 * @return
	 */
	public ExcelReader addStreamInterceptor(UnaryOperator<Stream<RowWrapper>> filter) {
		streamInterceptor = streamInterceptor.andThen(filter);
		return this;
	}
	
	public ExcelReader addConfig(Consumer<Workbook> config) {
		this.config = this.config.andThen(config);
		return this;
	}
	
	//--------------------------------- End setter ----------------------------------
	//------------------------- Start data fetcher methods --------------------------
	/** @return sheet name from according existing sheet index */
	public String getCurrentSheetName() throws EncryptedDocumentException, InvalidFormatException, IOException {
		return getSheetName(sheetIndex);
	}
	
	public String getSheetName(int sheetIndex) throws EncryptedDocumentException, InvalidFormatException, IOException {
		AtomicReference<String> sheetNameRef = new AtomicReference<String>();
		accessWB(wb->{
			String sheetName = wb.getSheetAt(sheetIndex).getSheetName();
			sheetNameRef.set(sheetName);
		});
		return sheetNameRef.get();
	}
	
	/**
	 * @param file
	 * @param length length of columns to get
	 * @return sheet data in list of list
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public List<List<String>> getAll(int length) throws EncryptedDocumentException, InvalidFormatException, IOException {
		return 
			getData(
				rowWrapper->IntStream
					.range(0, length)
					.mapToObj(rowWrapper::getCellWrapper)
					.map(CellWrapper::getValue)
					.collect(Collectors.toList())
			);
	}

	/**
	 * @param file
	 * @param colIndexArr indexes of columns
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public List<List<String>> getCols(int... colIndexArr) throws EncryptedDocumentException, InvalidFormatException, IOException {
		return 
			getData(
					rowWrapper->IntStream
						.of(colIndexArr)
						.mapToObj(rowWrapper::getCellWrapper)
						.map(CellWrapper::getValue)
						.collect(Collectors.toList())
			);
	}
	
	public List<String> getCol(char colChar) throws EncryptedDocumentException, InvalidFormatException, IOException {
//		return getCol(Character.toUpperCase(colChar)-'A');
		return getData(rowWrapper->rowWrapper.getCellWrapper(colChar).getValue());
	}
	public List<String> getCol(int colIndex) throws EncryptedDocumentException, InvalidFormatException, IOException {
		return getData(rowWrapper->rowWrapper.getCellWrapper(colIndex).getValue());
	}
	
	/** save/update file
	 * @param filter filter applicable for save only, does not effect other, but other stream are used here
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	public void save(UnaryOperator<Stream<RowWrapper>> filter) throws FileNotFoundException, IOException, EncryptedDocumentException, InvalidFormatException {
		Workbook wb = null;
		try {
			try(FileInputStream in = new FileInputStream(file)){
				wb = WorkbookFactory.create(in, filePassword);
			}
			
			config.accept(wb);
			
			Sheet sheet = getSheet(wb);
			streamInterceptor//2
				.andThen(filter)//3
				.apply(//1
					StreamSupport
						.stream(sheet.spliterator(), false)
						.map(RowWrapper::new)
				)
				.forEach(re->{});//4

			try(FileOutputStream out = new FileOutputStream(file)){
				wb.write(out);
			}
		} finally {
			if (wb != null)
				wb.close();
		}
	}
	//------------------------- End data fetcher methods ---------------------------
	
	//------------------------- Start core methods ---------------------------
	private <RowType> List<RowType> getData(Function<RowWrapper, RowType> rowDataCnvertor) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<RowType> list = new ArrayList<>();
		
		accessWB(wb->{
			Sheet sheet = getSheet(wb);
			streamInterceptor.apply(
					StreamSupport
						.stream(sheet.spliterator(), false)
						.map(RowWrapper::new)
				)
				.map(rowDataCnvertor)
				.forEach(list::add);
		});
		
		return list;
	}
	
	private Sheet getSheet(Workbook wb) {
		return sheetName != null ? wb.getSheet(sheetName) : wb.getSheetAt(sheetIndex);
	}
	
	private void accessWB(Consumer<Workbook> wbConsumer) throws EncryptedDocumentException, InvalidFormatException, IOException {
		try(Workbook wb = WorkbookFactory.create(file, filePassword, readOnly)){
			config.accept(wb);
			wbConsumer.accept(wb);
		}
	}
	
	//------------------------- End core methods ---------------------------
	
	public class RowWrapper {
		private Row row;

		private RowWrapper(Row row) {
			this.row = row;
		}

		public Row getRow() {
			return row;
		}
		
		/** @return row number (base=1) */
		public int getRowNum() {
			return row.getRowNum()+1;
		}
		
		private CellWrapper wrapCell(Cell cell) {
			return new CellWrapper(cell);
		}
		
		/** 
		 * @param colChar ['A'-'Z'], for limited cols
		 * @see #getCellWrapper(int) 
		 */
		public CellWrapper getCellWrapper(char colChar) {
			return getCellWrapper(Character.toUpperCase(colChar)-'A');
		}
		
		/**
		 * @param colIndex col index (base=0)
		 * @return cell wrapper
		 */
		public CellWrapper getCellWrapper(int colIndex) {
//			if(colIndex < 0)
//				throw new IndexOutOfBoundsException("Col index cannot be negative");
			return wrapCell(row.getCell(colIndex));
		}
		
		/** For each cell wrapper
		 * @param cellWrapperConsumer
		 */
		public void forEach(Consumer<CellWrapper> cellWrapperConsumer) {
			row.forEach(cell->cellWrapperConsumer.accept(wrapCell(cell)));
		}
		
		public void setCellVal(int colIndex, String value) {
			getCellWrapper(colIndex).setValue(value);
		}
		
		/**
		 * @param colIndex
		 * @return string value
		 * @see #getCellWrapper(int)
		 */
		public String getCellVal(int colIndex) {
			return getCellWrapper(colIndex).getValue();
		}
	}

	public class CellWrapper {
		private Cell cell;
		
		private CellWrapper(Cell cell) {
			this.cell = cell;
		}

		public Optional<Cell> getCell() {
			return Optional.ofNullable(cell);
		}

		private String value;
		public String getValue() {
			if (value == null)
				value = cellToStrConvertor.apply(cell);
			return value;
		}
		
		public void setValue(String value) {
			Objects.requireNonNull(cell, "Cell must not be null").setCellValue(value); //NullPointerException: should change missing cell policy of workbook or row
			this.value = null; //reset cached value
		}
		
		/** get background color */
		public java.awt.Color getBGColor(java.awt.Color def) {
			return getBGColor().orElse(def);
		}

		/** get background color */
		public Optional<java.awt.Color> getBGColor() {
			return
			getCell()
				.map(cell->cell.getCellStyle())
				.map((CellStyle style)->(ExtendedColor) style.getFillForegroundColorColor())
				.map((ExtendedColor color)->color.getARGB())
				.map((byte[] argb)->{
					int a = Byte.toUnsignedInt(argb[0]);
					int r = Byte.toUnsignedInt(argb[1]);
					int g = Byte.toUnsignedInt(argb[2]);
					int b = Byte.toUnsignedInt(argb[3]);
					return new java.awt.Color(r, g, b, a);
				});
		}

		@Override
		public String toString() {
			return getValue();
		}
	}

	
	/*
	//Example
	public static void main(String[] args) throws IOException, InvalidFormatException {
		File downloads = new File("C:\\Users\\dharmendra.chudasama\\Downloads");
//		File file = new File(downloads, "14022019 pending PSB_loan_14.02.2019.xls");
		File file = new File(downloads, "Data Fields for API Process v1.6_BOI.xlsx");
		
		new ExcelReader(file)
			.setSheetIndex(1)
			.setStreamInterceptor(
				stream->stream
//					.filter(rw->rw.getCell(1).getCellStyle()!=null)
//					.skip(2).limit(73)
			)
//			.getAll(file, 25)
//			.getCol(-1).stream().map(colData->colData.split(":")[1]) //lineNo
//			.getAll(10)
//			.stream().map(gstIn->qg.application(null, null, null, gstIn)).forEach(System.out::println);
			.getCol('C')
			.forEach(System.out::println);
		
//		System.out.println(buf);
	}
	*/

}
