package dharm.test.util;

import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.WeakHashMap;

public class Property {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("project"); //dot separate

	public static String get(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
//			return '!' + key + '!';
			throw e;
		}
	}
	
	private static final Map<String, String> cacheMap = new WeakHashMap<>(); //automatically clear when garbage collector called
	public static String get(String key, boolean decrypt) throws GeneralSecurityException {
		if(decrypt){
			String val = cacheMap.get(key); //use cache for ignore multiple time decryptions
			if(val==null)
				cacheMap.put(key, val=CryptoUtils.decrypt(get(key)));
			return val;
		}
		return get(key);
	}
}
