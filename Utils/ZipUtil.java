package dharm.test.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/** utility fir zip different files
 * @author Dharmendrasinh Chudasama
 */
public class ZipUtil {

	/**
	 * @param zipFile file to generate
	 * @param filesToZip file list to zip
	 * @return generatedZipFile
	 * @throws IOException
	 */
	public static File zipFiles(File zipFile, List<File> filesToZip) throws IOException {
		try( 
			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
		){
			for (File inputFile : filesToZip) {
				try (FileInputStream fis = new FileInputStream(inputFile)) {
					ZipEntry ze = new ZipEntry(inputFile.getName());
					zipOut.putNextEntry(ze);
					byte[] tmp = new byte[4 * 1024]; //4 KB at time
					int size = 0;
					while ((size = fis.read(tmp)) != -1) {
						zipOut.write(tmp, 0, size);
					}
					zipOut.flush();
				} //call close() methods of autoclosables->try(???...)
			}
		}
		return zipFile;
	}

	/**
	 * @param zip file to generate
	 * @param filesToZip
	 * @return Generated zip file (same as arg 1 after generation)
	 */
	public static File zipFiles(File zipFile, File...filesToZip) throws IOException {
		return zipFiles(zipFile, Arrays.asList(filesToZip));
	}

/*	*//** zip directory to same path
	 * @param dir
	 * @throws IOException
	 *//*
	public static void zipDir(File dir) throws IOException {
		if(!dir.isDirectory())
			throw new IllegalArgumentException("Directory not exists: "+dir.getAbsolutePath());

		File parent = dir.getParentFile();
		zipFiles(new File(parent, dir.getName()+".zip"), dir.listFiles());
	}
*/
/*	public static void main(String[] args) throws IOException {
		File dir = new File("/home/dchudasama/Desktop/ICICIGripsUtility/example_files");
		ZipUtil.zipDir(dir);
	}*/
}