package dharm.test.util;

import java.security.GeneralSecurityException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

/** Database utility
 * @author Dharmendrasinh Chudasama
 */
public class DatabaseUtil {
	private static final List<Connection> CONN_LIST = new LinkedList<>();
	
	@FunctionalInterface public interface Consumer<T>{ void accept(T data) throws Exception; }
	@FunctionalInterface public interface Function<T,R>{ R accept(T data) throws Exception; }
	
	static {
		try {
			Class.forName(Property.get("db_driver"));
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException("Can not load driver", ex);
		}

		// will call on shutdown JVM, example on System.exit(?);
		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			closeAllConnections();
		}, "Remaining connection closer thread hook"));
	}
	
	/** get db connection, for close connection use {@link #close(Connection)}, never call, conn.close()
	 * @return new database connection
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException, GeneralSecurityException {
//		Class.forName(Property.get("db_driver"));
		Connection conn = DriverManager.getConnection(Property.get("db_url", true), Property.get("db_username",true), Property.get("db_password",true));
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		CONN_LIST.add(conn);
		return conn;
	}
	
	public static void close(Connection conn) throws SQLException {
		if(conn != null){
			conn.close();
			CONN_LIST.remove(conn);
		}
	}

	/** close all remaining connection if any 
	 * @return 0=allClosed or remaining open connection due exceptions */
	public static synchronized int closeAllConnections(){ //close connections if open by mistake
		if(!CONN_LIST.isEmpty()) {
			for (Object conn : CONN_LIST.toArray()) { //toArray() for prevent concurrent modification exception
				try {
					close((Connection) conn);
				} catch (Exception e) {}
			}
		}
		return CONN_LIST.size();
	}

	/** hold in pool, don't worry will close automatic on shutdown JVM or call closeAllConnections() at the end of program */
	private static synchronized Connection getCachedReadOnlyConnection() throws ClassNotFoundException, SQLException, GeneralSecurityException {
		Connection conn = cachedReadOnlyConnection;
		if(conn==null || !conn.isValid(0)){
			cachedReadOnlyConnection = conn = getConnection();
			conn.setReadOnly(true);
		}
		return conn;
	}
	private static Connection cachedReadOnlyConnection = null;
	
	//========================= Data access Methods ================================================
	public static void execute(final Consumer<Connection> connConsumer) throws Exception {
		Connection conn = getConnection();
		try{
			connConsumer.accept(conn);
		}finally {
			close(conn);
		}
	}

	/** Method for handle transaction
	 * @param function
	 * @return returned value from function
	 */
	public static void transaction(final Consumer<Connection> allOrNoneConnConsumer) throws Exception {
		Connection conn = getConnection();
		conn.setAutoCommit(false);
		try{
			allOrNoneConnConsumer.accept(conn);
			conn.commit();
		}catch (Exception e) {
			conn.rollback();
			throw e;
		} finally {
			close(conn);
		}
	}
	
	/** @param arguments arguments for sql statement
	 * @param def default value or <code>null</code>
	 * @return first result column element, if not found def parameter value
	 */
/*	public static Object get(Connection conn, String sql, Object[] arguments, Object def) throws Exception {
		return select(conn, sql, arguments, rs->(rs.next() ? rs.getObject(1) : def));
	}
	public static Object get(String sql, Object[] arguments, Object def) throws Exception {
		return get(null, sql, arguments, def);
//		return execute(conn->get(conn, sql, arguments, def));
	}
*/
	
	/**
	 * @param conn instance of Connection or <code>null</code> for get cached conn
	 * @param sql
	 * @param arguments arguments or <code>null</code> for not found
	 * @param func
	 * @return argument func returned value
	 */
	public static <R> R select(Connection conn, String sql, Object[] arguments, Function<ResultSet, R> func) throws Exception {
		if(conn==null) conn = getCachedReadOnlyConnection();
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			setArgs(stmt, arguments);
			try (ResultSet rs = stmt.executeQuery()) {
				return func.accept(rs);
			}
		}
	}

	/** example: List<Map<String, Object>> list = DatabaseUtil.select(query, null, DatabaseUtil::toList); */
	public static <R> R select(String sql, Object[] arguments, Function<ResultSet, R> func) throws Exception {
		return select(null, sql, arguments, func);
//		return execute(conn->select(conn, sql, arguments, func));
	}
	
	/** execute any db query 
	 * @return return value of {@link Statement#executeUpdate(String)} */
	public static int query(final Connection conn, final String query, final Object... args) throws SQLException {
		if(args==null || args.length==0) //for better performance
			return conn.createStatement().executeUpdate(query);
		else {
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				setArgs(stmt, args);
				return stmt.executeUpdate();
			}
		}
	}

	/** Open connection, execute query, close connection
	 * @see #execute(Consumer)
	 * @see {@link #query(Connection, String, Object...)}
	 */
	public static int query(final String query, final Object... args) throws Exception {
		AtomicInteger ret = new AtomicInteger();
		execute(conn->ret.set(query(conn, query, args)));
		return ret.get();
	}

	/** @param conn
	 * @param insertQuery "INSERT INTO ..."
	 * @param args
	 * @return generated key or 0 if not generated
	 * @throws SQLException
	 */
	public static long insertRet(final Connection conn, final String insertQuery, final Object... args) throws SQLException {
		try (PreparedStatement stmt = conn.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)){
			setArgs(stmt, args);
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			return rs.next() ? rs.getLong(1) : 0l;
		}
	}
	
	/** access stored procedure
	 * @param conn
	 * @param spName name of stored procedure, definitely call
	 * @param args
	 * @param outBiConsumer for fetch out parameter call(indexOfSPParam, value), call once
	 * @param rsConsumer for fetch data (result sets), may call multiple time, ignore fetch data and call if any one null rsConsumer/updateCountConsumer
	 * @param updateCountConsumer for fetch data (update count), may call multiple time, ignore fetch data and call if any one null rsConsumer/updateCountConsumer
	 */
	private static void call(final Connection conn, final String spName, final Object[] args, final BiConsumer<String,Object> outBiConsumer, final Consumer<ResultSet> rsConsumer, final Consumer<Integer> updateCountConsumer) throws Exception {
		if(spName.toUpperCase().contains("CALL")) //TODO temp condition
			throw new IllegalArgumentException("pass SP name, not call query in argument");

		final Map<String, Object> spData = getSPData(conn, spName);
		final String sql = spData.get("sql").toString();
		@SuppressWarnings("unchecked") final List<Integer> outDataTypes = (List<Integer>) spData.get("outDataTypes");
		@SuppressWarnings("unchecked") final List<String> outColNames = (List<String>) spData.get("outColNames");

		try (final CallableStatement stmt = conn.prepareCall(sql)) {
			final int paramCount = stmt.getParameterMetaData().getParameterCount();
			final int afterAllInI = setArgs(stmt, args);
			
			final boolean hasOut = afterAllInI<=paramCount;
			if(hasOut) {
				final Iterator<String> outColNameItr = outColNames.iterator();
				final Iterator<Integer> outDataTypeItr = outDataTypes.iterator();
				while (outColNameItr.hasNext())
					stmt.registerOutParameter(outColNameItr.next(), outDataTypeItr.next());
//				for (int j = i; j<=paramCount; j++)
//					stmt.registerOutParameter(j, outDataTypeItr.next());
			}
			
			boolean hasRS = stmt.execute();//execute
			
			if(hasOut && outBiConsumer != null) {
				for (String colName : outColNames)
					outBiConsumer.accept(colName, stmt.getObject(colName));
			}
			
			final boolean fetchFlag = rsConsumer!=null && updateCountConsumer!=null;
			while(fetchFlag){ //validated logic
				if(hasRS){
					rsConsumer.accept(stmt.getResultSet());
				} else {
					final int count = stmt.getUpdateCount();
					if(count == -1) break;
					updateCountConsumer.accept(count);
				}
				hasRS = stmt.getMoreResults(Statement.CLOSE_CURRENT_RESULT);
			}
		}
	}

	/** each data from stored procedure
	 * @param conn
	 * @param spName
	 * @param args
	 * @return Map[outParamName, "RS"+rsIndex, "UC"+outIndex], indexBase=1
	 * 	 <li>Object retVal = map.get("P_RetVal");</li>
	 * 	 <li>List&lt;Map&lt;String, Object&gt;&gt; firstResultSet = map.get("RS1");</li>
	 * 	 <li>int firstUpdateCount = map.get("UC1");</li>
	 * </ul>
	 */
	public static Map<String, Object> callRetAll(final Connection conn, final String spName, final Object... args) throws Exception {
		AtomicInteger rsCounter = new AtomicInteger();
		AtomicInteger updateCountCounter = new AtomicInteger();

		Map<String, Object> map = new LinkedHashMap<>();
		call(conn, spName, args,
			(outParamName, value)->map.put(outParamName, value),
			(rs)->map.put("RS"+rsCounter.incrementAndGet(), toMapList(rs)),
			(updateCount)->map.put("UC"+updateCountCounter.incrementAndGet(), updateCount)
		);
		return map;
	}

	/** @return result set list */
	public static List<List<Map<String,Object>>> callRetRS(final Connection conn, final String spName, final Object... args) throws Exception {
		List<List<Map<String, Object>>> list = new LinkedList<>();
		call(conn, spName, args, null, rs->list.add(toMapList(rs)), updateCount->{});
		return list;
	}
	/** using normal connection for execute,
	 * @see #callRetRS(Connection, String, Object...) 
	 * @see #execute(Consumer)
	 * @return result set list */
	public static List<List<Map<String,Object>>> callRetRS(final String spName, final Object... args) throws Exception {
		List<List<Map<String, Object>>> list = new LinkedList<>();
		execute(conn->call(conn, spName, args, null, rs->list.add(toMapList(rs)), updateCount->{}));
		return list;
	}

	/** @return out param values, Map&lt;OutParamName, OutValue&gt; */
	public static Map<String, Object> callRetOut(final Connection conn, final String spName, final Object... args) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>(); //outParamName->value
		call(conn, spName, args, map::put, null, null);
		return map;
	}
	/** using transaction for execute
	 * @see #callRetOut(Connection, String, Object...) 
	 * @see #transaction(Consumer)
	 * @return out param values, Map&lt;OutParamName, OutValue&gt; */
	public static Map<String, Object> callRetOut(final String spName, final Object... args) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>(); //outParamName->value
		transaction(conn->call(conn, spName, args, map::put, null, null));
		return map;
	}

	/** call sp only */
	public static void call(final Connection conn, final String spName, final Object... args) throws Exception {
		call(conn, spName, args, null, null, null);
	}
	/** call sp only, using transactional connection
	 * @see #call(Connection, String, Object...) 
	 * @see #transaction(Consumer)
	 */
	public static void call(final String spName, final Object... args) throws Exception {
		transaction(conn->call(conn, spName, args));
	}

	
	//========================= Data Process Helper Methods ========================================
	/** set arguments to object
	 * @param stmt {@link PreparedStatement} or {@link CallableStatement}
	 * @see {@link #setArgs(PreparedStatement, int, Object...)}
	 * */
	public static int setArgs(final PreparedStatement stmt, final Object...args) throws SQLException {
		return setArgs(stmt, 1, args);
	}

	/**
	 * @param stmt {@link PreparedStatement} or {@link CallableStatement}
	 * @param from base=1 (first stmt.setObject(from++, arg);)
	 * @param args 
	 * @return next index which can set in <code>stmt.setObject(?, obj)</code>
	 * @throws SQLException
	 */
	public static int setArgs(final PreparedStatement stmt, int from, final Object...args) throws SQLException {
		if(args != null && args.length != 0)
			for (Object arg : args)
				stmt.setObject(from++, arg);
		return from;
	}

	/** some data of sp, note: it does not check sp existance
	 * @param conn database connection
	 * @param spName name of procedure
	 * @return Map [String sql, List&lt;Integer&gt; outDataTypes, List&lt;String&gt; outColNames]
	 */
	private static final Map<String, Object> getSPData(Connection conn, final String spName) throws ClassNotFoundException, SQLException, GeneralSecurityException {
		Map<String, Object> spData = getSPDataCache.get(spName);
		if (spData == null) { //fill if not found
			if(conn==null) conn = getCachedReadOnlyConnection();

			StringBuilder sql = new StringBuilder("{CALL ").append(spName.toString()).append('('); //toString for verify
			List<Integer> outDataTypes = new ArrayList<>();
			List<String> outColNames = new ArrayList<>();

			DatabaseMetaData metaData = conn.getMetaData();
//			boolean spFound = metaData.getProcedures(null, null, spName).next();
			ResultSet rs = metaData.getProcedureColumns(conn.getCatalog(), conn.getSchema(), spName, null);
			if(rs.next()) {
				sql.append('?');
				if(rs.getString("COLUMN_TYPE").equals("4")) {
					outDataTypes.add(rs.getInt("DATA_TYPE"));
					outColNames.add(rs.getString("COLUMN_NAME"));
				}

				while (rs.next()){
					sql.append(", ?");
					if(rs.getString("COLUMN_TYPE").equals("4")) { //is out parameter
						outDataTypes.add(rs.getInt("DATA_TYPE"));
						outColNames.add(rs.getString("COLUMN_NAME"));
					}
				}
			}

			spData = new HashMap<>();
			spData.put("sql", sql.append(")}").toString());
			spData.put("outDataTypes", outDataTypes);
			spData.put("outColNames", outColNames);
			getSPDataCache.put(spName, spData);
		}
		return spData;
	}
	private static final Map<String, Map<String, Object>> getSPDataCache = new WeakHashMap<>(); //ignores in garbage collector (no memory issue)

	/**
	 * @param rs resultset instance
	 * @return converted list from resultset 
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> toMapList(ResultSet rs) throws SQLException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		ResultSetMetaData meta = rs.getMetaData();
		int cols = meta.getColumnCount();
		Map<String, Object> map; // = new HashMap<>(cols);

		rs.beforeFirst();
		while (rs.next()) {
			map = new LinkedHashMap<>(cols);
			for(int i=1; i<=cols; i++)
				map.put(meta.getColumnLabel(i), rs.getObject(i));
			list.add(map);
		}
		return list;
	}

	/* * first column
	 * @param rs resultset instance (should has at least 1 column)
	 * @return first column
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	public static <E> List<E> toList(ResultSet rs) throws SQLException {
		List<E> list = new ArrayList<E>();
		rs.beforeFirst();
		while(rs.next()) 
			list.add((E) rs.getObject(1));
		return list;
	}

	/** 2 columns
	 * @param <K>
	 * @param <V>
	 * @param rs resultset instance (should has at least 2 columns)
	 * @return converted map<col1, col2> from resultset 
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> toMap(ResultSet rs) throws SQLException {
		Map<K,V> map = new LinkedHashMap<>();
		rs.beforeFirst();
		while(rs.next())
			map.put((K) rs.getObject(1), (V) rs.getObject(2));
		return map;
	}
	
/*	// generally in case of create excel
	public static String[] getLabelArray(final ResultSetMetaData meta) throws SQLException {
		final int cols = meta.getColumnCount();
		final String[] labels = new String[cols];

		for (int i = 0; i < cols; i++)
			labels[i] = meta.getColumnLabel(i+1);

		return labels;
	}
*/

}
