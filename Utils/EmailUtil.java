/*
 		<dependency>  <!-- Email -->
			<groupId>javax.mail</groupId>
			<artifactId>mail</artifactId>
			<version>1.4.5-rc1</version>
		</dependency>

 */
package dharm.test.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import org.apache.log4j.Logger;

/** Email utility
 * @author Dharmendrasinh Chudasama
 */
public class EmailUtil {
//	private static final Logger logger = Logger.getLogger(EmailUtil.class);
//	public static final int ATTACHMENT_MAX_SIZE_BYTE = 5242880;
	
	/**
	 * @param from from email id
	 * @param to email to comma(,) separated email IDs
	 * @param cc comma(,) separated email IDs or <code>null</code>
	 * @param bcc comma(,) separated email IDs or <code>null</code>
	 * @param subject email subject
	 * @param msgContent message content string
	 * @param isHTML <code>true</code> if msgContent is in HTML form, <code>false</code> when in plain text format
	 * @param attachments attached which length (file.length() < 5242880), others are ignored, and if has file than mail content will multipart
	 * @throws MessagingException
	 * @throws IOException errors related to accessing the attached file, if attached
	 */
	public static void sendEmail(String from, String to, String cc, String bcc, String subject, String msgContent, boolean isHTML, File... attachments) throws MessagingException, IOException {
		Transport transport = null;

		try{
			//Authenticating
			String host = Property.get("email_server_host");
			
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", Property.get("email_server_port"));
	
			Session session = Session.getInstance(props);
			transport = session.getTransport("smtp");
			transport.connect(host, Property.get("email_server_user"), Property.get("email_server_password"));
	
			//Sending email
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to) );
			if(cc!=null) message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc) );
			if(bcc!=null) message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc) );
			message.setSubject(subject);
	
	        String msgContentType = isHTML ? "text/html; charset=ISO-8859-1" : "text/text";
	        
	        if(attachments == null || attachments.length == 0)
				message.setContent(msgContent, msgContentType);
	        else { //multipart
				Multipart multipart = new MimeMultipart();
				
				MimeBodyPart contentBody = new MimeBodyPart();
				contentBody.setContent(msgContent, msgContentType);
	            multipart.addBodyPart(contentBody);
	
	        	for (File file : attachments)
	        		if(file!=null) {
//						if (file.length() <= ATTACHMENT_MAX_SIZE_BYTE) {
							MimeBodyPart filePart = new MimeBodyPart();
							filePart.attachFile(file);
						    multipart.addBodyPart(filePart);
/*		        		}else{
//	 	        			logger.warn("Can not attach file: "+file.getAbsolutePath());
						}*/
					}
	
	        	message.setContent(multipart);	
	        }
	
	        message.setSentDate(new Date());
	        message.saveChanges();
			transport.sendMessage(message, message.getAllRecipients());

//		}catch (Exception e) {
//			GEMFileXU.error(logger, "Can not send email", e);
		}finally {
			if(transport!=null && transport.isConnected()) transport.close();
		}
 	}
	
	/**
	 * @param from from email id
	 * @param to email to comma(,) separated email IDs
	 * @param subject email subject
	 * @param msgContent message content string
	 * @param isHTML <code>true</code> if msgContent is in HTML form, <code>false</code> when in plain text format
	 * @param attachments attached which length (file.length() < 5242880), others are ignored, and if has file than mail content will multipart
	 * @throws MessagingException
	 * @throws IOException if attached file and err occured from that
	 * @see #sendEmail(String, String, String, String, String, String, boolean, File...)
	 */
	public static void sendEmail(String from, String to, String subject, String msgContent, boolean isHTML, File... attachments) throws MessagingException, IOException {
		sendEmail(from, to, null, null, subject, msgContent, isHTML, attachments);
	}

}
