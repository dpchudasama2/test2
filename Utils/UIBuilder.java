package dharm.test;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 *  Utility for generate panel/dialog/frame with gridbag constraint
 * @author Dharmendrasinh Chudasama
 */
public class UIBuilder { //valid, almost done

	public static void main(String[] args) { //TODO for test only
		UIBuilder builder = new UIBuilder(0,0,0);
		builder.pair("First name", new JTextField());
		builder.pair("Sex", new JRadioButton("Male"), new JRadioButton("Female"));
		builder.drawLine();
		builder.label("Last name").fill(new JTextField()).newLine();
		JOptionPane.showMessageDialog(null, builder.getPanel());
		// TODO Auto-generated method stub
	}

	//==================== Instance logic & methods ======================

	private GridBagLayout layout;
	private JPanel panel;
//	layout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
//	layout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
	private int maxCol;
	private int rowNo = 0;
	private int colNo = 0;

	public UIBuilder(int...colMinWidths) {
		layout = new GridBagLayout();
		layout.columnWidths = colMinWidths;

		panel = new JPanel(layout);
		maxCol = colMinWidths.length;
		if(maxCol==0)
			throw new IllegalArgumentException("Provide column min widths (default=0)");
	}

	public GridBagLayout getLayout() {
		return layout;
	}

	public JPanel getPanel() {
		return panel;
	}

	public int getColCount() {
		return maxCol;
	}

	public int getRowNo() {
		return rowNo;
	}
	
	public int getColNo() {
		return colNo;
	}

	public int getRemainColCount() {
		return getColCount() - getColNo();
	}

	public void newLine() {
		rowNo++;
		colNo = 0;
	}

	private void assertTrue(boolean result, String errMsg){
		if(!result)
			throw new IllegalArgumentException(errMsg==null ? "Assertion failed" : errMsg);
	}
	private void assertHasRemaining(){
		if(getRemainColCount()<0)
			throw new ArrayIndexOutOfBoundsException("Overflow max col number maxCol="+maxCol+", current col="+colNo);
	}
	public void append(JComponent comp, GridBagConstraints constraints){
		colNo+=constraints.gridwidth;
		assertHasRemaining();
		constraints.gridy = rowNo;
		panel.add(comp, constraints);
	}
/*
	private GridBagConstraints getDefaultConstraints(){
		return new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0);
	}
*/
	/** horizontally fully fill fill  */
	private GridBagConstraints getFillConstraints(int gridWidth){
		return new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, gridWidth, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 0), 0, 0);
	}
	/** constraints for align center */
	private GridBagConstraints getCenterConstraints(){
		return new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0);
	}

	private GridBagConstraints getLabelConstraints(){
		return new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 10), 0, 0);
	}
	private GridBagConstraints getControlConstraints(int gridWidth){
		return new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, gridWidth, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0);
	}
	
	public UIBuilder label(String label){
		return label(new JLabel(label + " :"));
	}
	public UIBuilder label(JLabel label){
		append(label, getLabelConstraints());
		return this;
	}

	public UIBuilder control(JComponent component){
		return control(1, component);
	}
	public UIBuilder control(int gridWidth, JComponent component){
		append(component, getControlConstraints(gridWidth));
		return this;
	}
	
	/** Draw horizontal line and convert to next line*/
	public UIBuilder drawLine(){
		append(new JSeparator(), getFillConstraints(getRemainColCount()));
		newLine();
		return this;
	}
	
	/** Append full sized components */
	public UIBuilder fill(JComponent...components){
		int argLen = components.length;
		if(argLen==1)
			append(components[0], getFillConstraints(getRemainColCount()));
		else if(argLen > 1){
			int remains = getRemainColCount();
//			assertTrue(remains>=argLen, null);
			int eachWidth = remains / argLen;
			GridBagConstraints constraints = getFillConstraints(eachWidth);
			for (JComponent comp : components)
				append(comp, constraints);
		}
		return this;
	}
	
	public UIBuilder pair(String label, JComponent...components){
		this.label(label).fill(components).newLine();
		return this;
	}
	//======================== static methods ==========================
	/** @return new panel with flow layout */
	public static JPanel groupPane(int flowLayoutAlign, JComponent...components){
		JPanel p = new JPanel(new FlowLayout(flowLayoutAlign));
		for (JComponent comp : components) p.add(comp);
		return p;
	}

}
